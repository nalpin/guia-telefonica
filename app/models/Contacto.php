<?php

class Contacto extends Eloquent {
	
	protected $fillable = array('nombre', 'telefono', 'email', 'direccion');

	public function usuario() {
		return $this->belongsTo('User');
	}
}