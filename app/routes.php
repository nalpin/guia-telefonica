<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', [
	'as' => 'home',
	'uses' => 'HomeController@home'
	]);

//Para poder ejecutar migraciones en shared host
Route::get('/migrate', function() {
   Artisan::call('migrate', array('--force' => 'true'));
});

// Usuarios autenticados
Route::group(['before' => 'auth'], function () {

	Route::group(['before' => 'csrf'], function() {

		Route::post('/user/change-password', [
			'uses' => 'UserController@postChangePassword'
			]);

		Route::put('/user', [
			'as' => 'user.update',
			'uses' => 'UserController@update'
			]);

		Route::resource('contacto', 'ContactoController',
			['only' => ['store', 'update', 'destroy']]);	

	});

	Route::get('/user/logout', [
		'as' => 'user.logout',
		'uses' => 'UserController@getLogout'
		]);			

	Route::get('/user/change-password', [
		'as' => 'user.change-password',
		'uses' => 'UserController@getChangePassword'
		]);

	Route::get('/user/edit', [
		'as' => 'user.edit',
		'uses' => 'UserController@edit'
		]);

	Route::get('/contacto/{contacto}/delete', [
		'as' => 'contacto.delete',
		'uses' => 'ContactoController@delete'
		]);

	Route::resource('contacto', 'ContactoController',
		['except' => ['store', 'update', 'destroy']]);
});

// Usuarios no autenticados
Route::group(['before' => 'guest'], function() {

	Route::group(['before' => 'csrf'], function() {
		
		Route::post('/user/login', [
			'uses' => 'UserController@postLogin'
			]);

		Route::post('/user/reset', [
			'uses' => 'UserController@postReset'
			]);

		Route::resource('user', 'UserController', 
			['only' => ['store']]);
	});

	Route::get('/user/login', [
		'as' => 'user.login',
		'uses' => 'UserController@getLogin'
		]);

	Route::get('/user/login/fb', [
		'as' => 'login.fb',
		'uses' => 'UserController@loginFB'
		]);

	Route::get('/user/login/fb/callback', [
		'as' => 'login.fb.callback',
		'uses' => 'UserController@loginFBCallback'
		]);

	Route::get('/user/reset', [
		'as' => 'user.reset',
		'uses' => 'UserController@getReset'
		]);

	Route::resource('user', 'UserController',
		['only' => ['create']]);

});

/*
Route::resource('user', 'UserController',
	['except' => ['index']]);
*/