<!-- Modal -->
<div class="modal fade" id="delContacto{{$contacto->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Confirmar Eliminación</h4>
      </div>
      <div class="modal-body">
        <h3>Presione confirmar si esta seguro de eliminar al contacto 
        <strong> {{ $contacto->nombre }} </strong>
        </h3>
      </div>
      <div class="modal-footer">
        {{ Form::open(['route' => ['contacto.destroy', $contacto->id], 'method' => 'DELETE']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        {{ Form::submit('Confirmar', ['class' => 'btn btn-info']) }}
        {{ Form::close() }}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->          