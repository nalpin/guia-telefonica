@extends('layout.base')

@section('titulo')
Contactos
@stop

@section('contenido')
	{{ link_to_route('contacto.create', 'Agregar contacto', [], ['class' => 'btn btn-primary']) }}
	@if ($contactos->isEmpty())
		<div class="alert alert-danger alert-dismissible" role="alert">
  			<button type="button" class="close" data-dismiss="alert">
  			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  			Usuario no tiene contactos!
		</div>
	@else
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Telefono</th>
				<th>Direccion</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
			@foreach($contactos as $contacto)
				<tr>
					<td> {{ $contacto->nombre }} </td>
					<td> {{ $contacto->telefono }} </td>
					<td> {{ $contacto->direccion }} </td>
					<td> {{ $contacto->email }} </td>
					<td> {{ link_to_route('contacto.edit', 'Editar', [$contacto->id], 
						['class' => 'btn btn-info']) }} </td>
					<td>
						<button class="btn btn-warning" data-toggle="modal" 
							data-target="#delContacto{{$contacto->id}}" >
						  Eliminar
						</button>
						@include('contacto.delete', ['contacto' => $contacto])
					</td>
				</tr>
			@endforeach		
		</tbody>
	</table>
	
	@endif
@stop
