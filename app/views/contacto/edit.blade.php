@extends('layout.base')

@section('titulo')
Editar Contacto
@stop

@section('contenido')
	{{Form::model($contacto, ['route' => ['contacto.update', $contacto->id], 'method' => 'PUT', 
		'class' => 'form-horizontal','role' => 'form'])}}
	<legend>Editar Contacto</legend>
	@if($errors->has('nombre'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('nombre', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('nombre', 'Nombre', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('nombre', $contacto->nombre, ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('telefono'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('telefono', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('telefono', 'Telefono', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('telefono', $contacto->telefono, ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('direccion'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('direccion', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('direccion', 'Direccion', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('direccion', $contacto->direccion, ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('email'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('email', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('email', 'Email', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::email('email', $contacto->email, ['class' => 'form-control'])}}
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
		{{Form::submit('Actualizar', ['class' => 'btn btn-success btn-lg btn-block'])}}
		</div>
	</div>
	{{Form::close()}}
@stop