<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title> 
	@section('titulo') 
	Agenda
	@show
	</title>
	<!-- Referencing Bootstrap CSS that is hosted locally -->
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/bootstrap-theme.css') }}
    {{ HTML::style('css/custom.css') }}
</head>
<body>
	
	@include('layout.navigation')

	@if(Session::has('success'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			{{ Session::get('success') }}
		</div>
	@endif
	@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissible" role="alert">
  			<button type="button" class="close" data-dismiss="alert">
  			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  			<strong>Error!</strong> {{ Session::get('error') }}
		</div>
	@endif

	@section('encabezado')
	@show
	
	<div id="contenido" class="container">
		@section('contenido')
		@show
	</div>

	@section('pie')
		@include('layout.footer')
	@show

	{{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    @section('script')
    @show
</body>
</html>