<div class="container-fluid">
{{--
<div class="jumbotron">
    <h1>Tu Guía Telefónica en la Nube</h1>
    <p>Registrate hoy totalmente gratis.</p>
    {{ link_to_route('user.create', 'Registro', [], ['class' => 'btn btn-primary btn-lg']) }}
</div>
--}}

 	<!-- Carousel
    ================================================== -->
	<div class="row-fluid">
	  <div class="span12">
	    <div id="myCarousel" class="carousel slide">
	      <!-- Indicators -->
      	  <ol class="carousel-indicators">
	        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	        <li data-target="#myCarousel" data-slide-to="1"></li>
      	  </ol>
	      <div class="carousel-inner">
	        <div class="item active">
	          <img src="images/phone-book1.jpg" alt="">
	          <div class="container">
	            <div class="carousel-caption">
	          		<h4>Tu Guía Telefónica en la Nube</h4>
	    			<p>Registrate hoy totalmente gratis.</p>
	    			{{ link_to_route('user.create', 'Registro', [], ['class' => 'btn btn-primary btn-lg']) }}
	            </div>
	          </div>
	        </div>
	        <div class="item">
	          <img src="images/phone-book2.jpg" alt="">
	          <div class="container">
	            <div class="carousel-caption">
	          		<h4>Su Privacidad es Nuestra Prioridad</h4>
	    			<p>No revelamos ni proporcionamos información sensible a terceros.</p>
	    			
	            </div>
	          </div>
	        </div>
	      </div><!-- carousel-inner -->
	      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	      	<span class="glyphicon glyphicon-chevron-left"></span></a>
  		  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
  		  	<span class="glyphicon glyphicon-chevron-right"></span></a>
	    </div><!-- /.carousel -->
	  </div>
	</div>
</div>