<footer>
  <div class="container">
  <div class="row">
    <div class="col-md-4">
      <ul class="nav nav-pills">
        <li class="active"><a href="#">Acerca de</a></li>
        <li><a href="#">Soporte</a></li>
        <li><a href="#">Política de privacidad</a></li>
      </ul>
    </div>
    <div class="col-md-4">
      <h3 class="text-right">Agenda 2.0</h3>
    </div>
    <div class="col-md-4">
      <p class="text-right">Napsoft &copy; 2014</p>
    </div>
  </div>
  </div>
</footer>