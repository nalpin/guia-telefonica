@extends('layout.base')

@section('titulo')
Acceder al sistema
@stop

@section('contenido')
	{{Form::open(['action' => 'UserController@postLogin', 'class' => 'form-signin' , 'role' => 'form'])}}
	<legend class="form-signin-heading">Acceder al Sistema</legend>
	@if($errors->has('usuario'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('usuario', '<span class="help-block">:message</span>') }}
		{{Form::text('usuario', '', ['class' => 'form-control', 'placeholder' => 'Nombre de usuario'])}}
	</div>
	@if($errors->has('password'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('password', '<span class="help-block">:message</span>') }}
		{{Form::password('password', ['class' => 'form-control', 'placeholder' => 'Contraseña'])}}
	</div>
	<div class="checkbox ">
	<div class="col-lg-offset-1">
	{{Form::checkbox('remember')}}
	{{Form::label('remember', 'No cerrar sesion')}}
	</div>
	</div>
	<div class="form-group">
		<div class="col-lg-offset-1">
		{{ link_to_route('user.reset', '¿Olvidaste tu contraseña?', [],
			['class' => 'btn btn-warning btn-sm']) }}
		</div>
	</div>
	{{Form::submit('Acceder', ['class' => 'btn btn-success btn-lg btn-block'])}}
	{{Form::close()}}
@stop