@extends('layout.base')

@section('titulo')
Agenda
@stop

@section('contenido')
	{{Form::open(['action' => 'UserController@postReset', 'class' => 'form-signin' , 'role' => 'form'])}}
	<legend class="form-signin-heading">Recuperar Cuenta</legend>
	@if($errors->has('email'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('email', '<span class="help-block">:message</span>') }}
		{{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Correo Electronico'])}}
	</div>
	{{Form::submit('Enviar', ['class' => 'btn btn-success btn-lg btn-block'])}}
	{{Form::close()}}
@stop