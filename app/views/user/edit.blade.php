@extends('layout.base')

@section('titulo')
Editar Perfil
@stop

@section('contenido')
	{{Form::model($usuario, ['route' => ['user.update'], 'method' => 'PUT', 
		'class' => 'form-horizontal','role' => 'form'])}}
	<legend>Editar Perfil</legend>
	@if($errors->has('nombre'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('nombre', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('nombre', 'Nombres', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('nombre', $usuario->nombre, ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('apellidos'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('apellidos', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('apellidos', 'Apellidos', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('apellidos', $usuario->apellidos, ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('dpi'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('dpi', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('dpi', 'DPI', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('dpi', $usuario->dpi, ['class' => 'form-control'])}}
		</div>
	</div>
	<div class="form-group">
		{{Form::label('usuario', 'Usuario', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('usuario', $usuario->usuario, ['class' => 'form-control', 'disabled'])}}
		</div>
	</div>
	<div class="form-group">
		{{Form::label('email', 'Email', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::email('email', $usuario->email, ['class' => 'form-control', 'disabled'])}}
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
		{{ link_to_route('user.change-password', 'Cambiar Contraseña', [],
			['class' => 'btn btn-warning btn-lg']) }}
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
		{{Form::submit('Actualizar', ['class' => 'btn btn-success btn-lg btn-block'])}}
		</div>
	</div>
	{{Form::close()}}
@stop