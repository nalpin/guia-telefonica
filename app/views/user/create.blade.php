@extends('layout.base')

@section('titulo')
Registro
@stop

@section('contenido')
	{{Form::open(['route' => ['user.store'], 'class' => 'form-horizontal','role' => 'form'])}}
	<legend>Registro</legend>
	@if($errors->has('nombre'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('nombre', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('nombre', 'Nombres', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('nombre', '', ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('apellidos'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('apellidos', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('apellidos', 'Apellidos', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('apellidos', '', ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('dpi'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('dpi', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('dpi', 'DPI', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('dpi', '', ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('usuario'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('usuario', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('usuario', 'Usuario', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::text('usuario', '', ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('email'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('email', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('email', 'Email', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::email('email', '', ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('password'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('password', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('password', 'Contraseña', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::password('password', ['class' => 'form-control'])}}
		</div>
	</div>
	<div class="form-group">
		{{Form::label('password_confirmation', 'Comprobar contraseña', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::password('password_confirmation', ['class' => 'form-control'])}}
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
		{{Form::submit('Guardar', ['class' => 'btn btn-success btn-lg btn-block'])}}
		</div>
	</div>
	{{Form::close()}}
@stop