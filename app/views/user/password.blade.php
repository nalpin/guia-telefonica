@extends('layout.base')

@section('titulo')
Editar Perfil
@stop

@section('contenido')
	{{Form::open(['route' => ['user.change-password'], 'class' => 'form-horizontal','role' => 'form'])}}
	@if($errors->has('old_password'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('old_password', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('old_password', 'Contraseña actual', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::password('old_password', ['class' => 'form-control'])}}
		</div>
	</div>
	@if($errors->has('password'))
	<div class="form-group has-error">
	@else
	<div class="form-group">
	@endif 
		{{ $errors->first('password', '<span class="help-block col-lg-offset-2">:message</span>') }}
		{{Form::label('password', 'Contraseña nueva', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::password('password', ['class' => 'form-control'])}}
		</div>
	</div>
	<div class="form-group">
		{{Form::label('password_confirmation', 'Comprobar contraseña', ['class' => 'col-lg-2 control-label'])}}
		<div class="col-lg-10">
		{{Form::password('password_confirmation', ['class' => 'form-control'])}}
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-offset-2 col-lg-10">
		{{Form::submit('Guardar', ['class' => 'btn btn-success btn-lg btn-block'])}}
		</div>
	</div>
	{{Form::close()}}
@stop