@extends('layout.base')

@section('titulo')
Agenda
@stop

@section('encabezado')
	@include('layout.header')
@stop

@section('contenido')
@stop

@section('script')
<script>
	$('#myCarousel').carousel({
		interval: 4000,
		cycle: true
	});
</script>
@stop