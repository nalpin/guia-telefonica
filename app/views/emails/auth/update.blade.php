<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{ $nombre }}. Su cuenta ha sido actualizada</h2>

		<div>
			No divulge su nombre de usuario ni sus contraseñas.
		</div>
	</body>
</html>
