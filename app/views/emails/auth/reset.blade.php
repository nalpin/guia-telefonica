<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Solicitud de recuperación de cuenta.</h2>

		<div>
			<p>
			Parece que Ud. solicitó la recuperación de su cuenta a este correo electrónico.
			Por favor ingrese al sistema con las siguientes credenciales:
			</p>
			<p>Usuario    : {{ $usuario }}</p>
			<p>Contraseña : {{ $password }}</p><br>
			<p>Al siguiente enlace:  {{ route('user.login') }}</p>
		</div>

	</body>
</html>
