<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Cuenta de {{ $usuario }} ha sido creada exitosamente!</h2>

		<div>
			No divulge su nombre de usuario ni sus contraseñas.
		</div>
	</body>
</html>
