<?php

class ContactoController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//GET contacto
		//$contactos = Auth::user()->contactos()->paginate(50);
		$usuario = Auth::user();
		$contactos = $usuario->contactos()->get();
		//dd(DB::getQueryLog());
		return View::make('contacto.index', ['contactos' => $contactos]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//GET contacto/create
		return View::make('contacto.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//POST contacto
		$data = Input::all();
		$rules = [
			'nombre' => 'required',
			'telefono' => 'required',
			'direccion' => 'required',
			'email' => 'required|max:60|email'
			];

		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {
			return Redirect::route('contacto.create')
				->withErrors($validator)->withInput();
		}
		$usuario = Auth::user();

		$contacto = new Contacto;

		$contacto->nombre = $data['nombre'];
		$contacto->telefono = $data['telefono'];
		$contacto->direccion = $data['direccion'];
		$contacto->email = $data['email'];
		$contacto->usuario()->associate($usuario);

		if($contacto->save()) {
			return Redirect::route('contacto.index')
        		->with('success','Contacto agregado');
    	}
    	
		return Redirect::route('contacto.index')
			->with('error','No se pudo guardar contacto');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//GET contacto/{$id}
		$contacto = Contacto::find($id);
		return View::make('contacto.show', ['contacto' => $contacto]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//GET contacto/{$id}/edit
		$contacto = Contacto::find($id);
		return View::make('contacto.edit', ['contacto' => $contacto]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//PUT contacto/{$id}
		$data = Input::all();
		$rules = [
			'nombre' => 'required',
			'telefono' => 'required',
			'email' => 'required|max:60|email'
			];

		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {
			return Redirect::route('contacto.edit')
				->withErrors($validator)->withInput();
		}

		$contacto = Contacto::find($id);

		$contacto->nombre = $data['nombre'];
		$contacto->telefono = $data['telefono'];
		$contacto->direccion = $data['direccion'];
		$contacto->email = $data['email'];

		if($contacto->save()) {
			return Redirect::route('contacto.index')
        		->with('success','Contacto actualizado');
    	}
    	
		return Redirect::route('contacto.index')
			->with('error','No se pudo actualizar contacto');

	}


	// Confirmation page for delition
	//
	public function delete($id) {
		//GET contacto/{$id}/delete
		$contacto = Contacto::find($id);
		if($contacto) {
			return View::make('contacto.delete', ['contacto' => $contacto]);		
		}
		return Redirect::route('contacto.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//DELETE contacto/{$id}
		$contacto = Contacto::find($id);
		if($contacto->delete()) {
			return Redirect::route('contacto.index')
        		->with('success','Contacto eliminado');
    	}
    	
		return Redirect::route('contacto.index')
			->with('error','No se pudo eliminar contacto');		
	}


}
