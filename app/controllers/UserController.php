<?php

class UserController extends BaseController {

	/*
	 * 
	*/
	public function getLogin() {
		//GET user/login
		return View::make('user.login');
	}

	public function loginFB() {
		//GET user/login/fb
		$facebook = new Facebook(Config::get('facebook'));
		//$url = route('login.fb.callback');
		//dd($url);
	    $params = array(
	        'redirect_uri' => url('/user/login/fb/callback'),
	        'scope' => 'email',
	    );
	    return Redirect::to($facebook->getLoginUrl($params));
	}

	public function loginFBCallback() {
	    
	    $code = Input::get('code');
	    
	    if (strlen($code) == 0) {
	    	return Redirect::route('home')
	    		->with('error', 'Hubo un error al intentar conectarse con Facebook');
    	}

	    $facebook = new Facebook(Config::get('facebook'));
	    $uid = $facebook->getUser();

	    if ($uid == 0) {
	    	return Redirect::route('home')
	    		->with('error', 'Hubo un error en la respuesta de Facebook');	
	    } 

	    $me = $facebook->api('/me');

	    dd($me);
	}
	/*
	 * 
	*/
	public function getReset() {
		//GET user/reset
		return View::make('user.reset');
	}

	/*
	 * 
	*/
	public function getLogout() {
		//GET user/logout
		Auth::logout();
		Session::flush();
		return Redirect::route('home');
	}

	/*
	 * 
	*/
	public function postLogin() {
		//POST user/login
		
		$data = Input::all();
		$rules = [
			'usuario' => 'required',
			'password' => 'required'
			];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			return Redirect::route('user.login')
				->withErrors($validator)->withInput();
		}
		// Procesar Login
		$usuario = [
            'usuario' => $data['usuario'],
            'password' => $data['password']
        ];

        $remember = Input::has('remember');

		$auth = Auth::attempt($usuario, $remember);

		if ($auth) {
			// TODO: Send Email
			// Mail::send();
        	return Redirect::intended(route('home'));
    	}

    	return Redirect::route('user.login')
    		->with('error', 'Usuario o contraseña incorrectos');
    	
	}

	/*
	 * 
	*/
	public function postReset() {
		//POST user/reset
		
		$data = Input::all();
		$rules = [
			'email' => 'required|email'
			];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			return Redirect::route('user.reset')
				->withErrors($validator)->withInput();
		}
		
		$email = $data['email'];

		$usuario = User::where('email', '=', $email)->first();

		if ($usuario) {
			$password = str_random(10);
			$usuario->password = Hash::make($password);
			if ($usuario->save()) {
				// TODO: Send Email
				Mail::send('emails.auth.reset', 
					['usuario' => $usuario->usuario, 'password' => $password], 
					function($message) use($usuario) {
						$message->to($usuario->email, $usuario->usuario)->subject('Solicitud de recuperacón');
				});
				return Redirect::route('home')
	    			->with('success', 'Se ha enviado la solicitud al email proporcionado.');        	
			}
    	} else {
			return Redirect::route('user.reset')
    			->with('error', 'No se encontró el email proporcionado.');    		
    	}

    	return Redirect::route('user.reset')
    		->with('error', 'No se pudo recuperar la cuenta.');
    	
	}
	/*
	 * 
	*/
	public function getChangePassword() {
		//GET user/change-password
		return View::make('user.password');
	}

	/*
	 * 
	*/
	public function postChangePassword() {
		//POST user/change-password
		$data = Input::all();
		$rules = [
			'old_password' => 'required',
			'password' => 'required|confirmed'
			];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			return Redirect::route('user.change-password')
				->withErrors($validator);
		}

		$usuario = Auth::user();
		
		if (Hash::check($data['old_password'], $usuario->password)) {
			$usuario->password = Hash::make($data['password']);	
			
			if($usuario->save()) {
				Mail::send('emails.auth.update', ['nombre' => $usuario->nombre], function($message) use($usuario) {
					$message->to($usuario->email, $usuario->nombre)->subject('Cambio de contraseña');
				});
    	    	return Redirect::route('home')
        			->with('success','Contraseña ha sido cambiada.');
    		}
		} else {
			return Redirect::route('user.change-password')
    			->with('error','Contraseña actual incorrecta');
		}
		return Redirect::route('user.change-password')
			->with('error','Ha ocurrido un error al intentar cambiar contraseña.');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//GET user/create
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//POST user
		$data = Input::all();
		$rules = [
			'nombre' => 'required|max:50',
			'apellidos' => 'required|max:70',
			'dpi' => 'required|numeric|min:10',
			'email' => 'required|max:60|email|unique:usuarios',
			'usuario' => 'required|max:20|min:3|unique:usuarios',
			'password' => 'required|confirmed'
			];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			return Redirect::route('user.create')
				->withErrors($validator)->withInput();
		}
		$usuario = new User;
		//dd($data);
		$usuario->nombre = $data['nombre'];
		$usuario->apellidos = $data['apellidos'];
		$usuario->dpi = $data['dpi'];
		$usuario->usuario = $data['usuario'];
		$usuario->email = $data['email'];
		$usuario->password = Hash::make($data['password']);
		
		if($usuario->save()) {
			// TODO: Send Email
			Mail::send('emails.auth.create', ['usuario' => $usuario->usuario], function($message) use($usuario) {
				$message->to($usuario->email, $usuario->nombre)->subject('Registro exitoso.');
			});
        	return Redirect::route('home')
        		->with('success','Usuario guardado');
    	}
    	
		return Redirect::route('home')
			->with('error','No se pudo guardar usuario');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit()
	{
		//GET user/edit
		$usuario = Auth::user();
		return View::make('user.edit', ['usuario' => $usuario]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//PUT user
		$data = Input::all();
		$rules = [
			'nombre' => 'required|max:50',
			'apellidos' => 'required|max:70',
			'dpi' => 'required'
			];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			return Redirect::route('user.edit')
				->withErrors($validator)->withInput();
		}

		$usuario = Auth::user();

		$usuario->nombre = $data['nombre'];
		$usuario->apellidos = $data['apellidos'];
		$usuario->dpi = $data['dpi'];
		
		if($usuario->save()) {
			// TODO: Send Email
			Mail::send('emails.auth.update', ['nombre' => $usuario->nombre], function($message) use($usuario) {
				$message->to($usuario->email, $usuario->nombre)->subject('Cuenta actualizada');
			});
			return Redirect::route('home')
        		->with('success','Usuario guardado');
    	}
    	
		return Redirect::route('home')
			->with('error','No se pudo guardar usuario');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
